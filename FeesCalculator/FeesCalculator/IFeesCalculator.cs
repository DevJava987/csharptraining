﻿using System.ServiceModel;


namespace FeesCalculator
{
    //ServiceContract attribute describes that this interface contains the methods signatures of WCF service
    [ServiceContract]//using System.ServiceModel
    interface IFeesCalculator
    {
        //OperationContract attribute describes that this method is included in WCF service and it is accessible to the client. Without OperationContract attribute on method, a client can't access this service method
        [OperationContract]
        double ComputeFees(string clientId, string marketId, string activityId);


        [OperationContract]
        Fees GetFees(string clientId, string marketId, string activityId);
    }
}