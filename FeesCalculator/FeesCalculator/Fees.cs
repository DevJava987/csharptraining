﻿using System.Runtime.Serialization;

namespace FeesCalculator
{
    [DataContract]
    class Fees
    {
        [DataMember]
        public double Bips;
        [DataMember]
        public double Min;
        [DataMember]
        public double Max;
    }
}