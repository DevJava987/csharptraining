﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeesCalculator
{
    class FeesCalculator : IFeesCalculator
    {
        private static int counter = 0;
        public double ComputeFees(string clientId, string marketId, string activityId)
        {
            counter++;
            return 0.5;
        }

        public Fees GetFees(string clientId, string marketId, string activityId)
        {
            counter++;
            return new Fees() { Bips = 1.5, Min = 5, Max = 10 };
        }
    }
}