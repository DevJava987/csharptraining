﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FeesCalculator
{
    class Program
    {
        // ####################   code inspired from this url ################
        /// <summary>
        /// http://dotnetpattern.com/hello-world-wcf
        /// </summary>
        public static void Main(string[] args)
        {
            ServiceHost server = new ServiceHost(typeof(FeesCalculator));
            server.Open();

            Console.WriteLine("Service FeesCalculator is running...");
            Console.ReadLine();
            server.Close();
        }
    }
}