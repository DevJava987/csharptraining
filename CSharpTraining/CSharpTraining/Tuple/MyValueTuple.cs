﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.Tuple
{
    /**
     * A ValueTuple introduced in C# 7 (.NET 4.7) is a valueType reprensentation of a Tuple.
     * 
     * - It is not systematically allocated on the heap (less CPU interaction in case of simple Tuple)
     * - can contain more than eight elements
     * - idiot-proof syntax
     * - its element can ben named (more readable than Item1, Item2...)
     * 
     **/
    class MyValueTuple
    {
        public static void Main()
        {
            //######## idiot-proof syntax #######
            var firstTuple = (1, 2, 3);
            Console.WriteLine(firstTuple);

            ValueTuple<int, int, int> secondTuple = new ValueTuple<int, int, int>(1, 2, 3);
            //Pay attention
            var notTuple = (1);//is not a tuple, it is simply an int
            Console.WriteLine(notTuple);


            //######### named elements #########
            //When no name is specified, elements are accessed by Item1, Item2...
            var namedTuple = (Id: 1, Id3: 3, Id2: 2);
            Console.WriteLine(namedTuple);

            (int Id1, int Id2, int Id3) namedTupleV2 = (Id1: 1, Id2: 2, Id3: 3);
            Console.WriteLine(namedTupleV2);

        }
    }
}
