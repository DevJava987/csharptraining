﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.MultipleImplementation
{
    class MyClass : INterfaceOne, INterfaceTwo
    {
        public void Process()
        {
            throw new NotImplementedException();
        }

        void INterfaceOne.Process() => Console.WriteLine("Method of INterfaceOne");

        void INterfaceTwo.Process()
        {
            Console.WriteLine("Method of INterfaceOne");
        }

        public void Main()
        {
            //Since myClass1 is of type MyClass the first Process method will be called
            //None of the explicit implementation methods "INterfaceOne.Process()" & "INterfaceTwo.Process()"
            // will be called in this case.
            MyClass myClass1 = new MyClass();
            myClass1.Process();

            INterfaceOne i1 = new MyClass();
            i1.Process(); //The method process inherited from INterfaceOne is called here

            INterfaceTwo i2 = new MyClass();
            i2.Process(); // the method process inherited from INterfaceTwo is called here

        }
    }
}