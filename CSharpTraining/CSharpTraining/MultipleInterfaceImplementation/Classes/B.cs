﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.MultipleImplementation.Classes
{
    class B : A
    {
        //B inherits the method "void Process()" from class A
    }
}