﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.Interfaces
{
    /// <summary>
    /// A extension  method should be defined in a static class.
    /// The method should have also the "static" modifier
    /// 
    /// </summary>
    static class StaticTestUser
    {
        public static string ExtensionMethod(this ITestUser testUser)
        {
            //some code here
            testUser.FormatName();
            Console.WriteLine("ExtensionMethod");
            return "coucou";
        }

    }
}
