﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.Interfaces
{
    /// <summary>
    /// Interfaces in C# do not allow default implementation for methods (but it will be available starting from C#8.
    /// However using extension method for the interface mkae this indirectly possible.
    /// </summary>
    interface ITestUser
    {
        //Note that interfaces can note have fields.
        //But can have Properties (setter and getter)
        //If there's no special logic associated with the property: implementing classes can use auto-properties to simplify implementation 
        int Id { get; set; }
        string FirstName { get; set; }
        string Name { get; set; }

        //Can not put "public" modifier here, compilation error!
        //methods defined in interface are public by default!
        string FormatName();
    }
}
