﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.Interfaces
{
    class TestUserImpl : ITestUser
    {
        public int Id { get; set; }
        public string FirstName { get { return "string"; } set { } }
        public string Name { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public string FormatName()
        {
            throw new NotImplementedException();
        }

        public static void Main()
        {
            TestUserImpl user = new TestUserImpl();

            //Call the extension method as if it were inhereted from the interface
            user.ExtensionMethod();
        }
    }
}
