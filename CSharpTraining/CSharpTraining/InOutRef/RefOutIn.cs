﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.InOutRef
{
    //https://www.c-sharpcorner.com/UploadFile/ff2f08/ref-vs-out-keywords-in-C-Sharp/
    // - The in/ref/out key word causes arguments to be passed by reference.
    // - Unlike ref/out, the in argument can not be modified in the called method (compilation error).
    // - Ref argument can be modified in the called method but it is not mandatory to do so.
    //   Ref argument must be initilazed in the calling method.
    // - out argument must be assigned int the called method.

    // Method overloading: The in, ref, and out keywords are not considered part of the method signature
    // for the purpose of overload resolution. 
    // Therefore, methods cannot be overloaded if the only difference is that one method takes a ref or in 
    // argument and the other takes an out argument.

    class RefOutIn
    {
    }
}