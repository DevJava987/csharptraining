﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Tests.Destructor
{
    class MyObject
    {

        private readonly Stopwatch sw;

        public MyObject()
        {
            //Use resources here
            sw = Stopwatch.StartNew();
        }

        // Finalizer (aka destructor) is called when the object is collected by the garbage collector
        // The programmer has no control over when the finalizer is called because this is determined
        // by the GC. The GC checks for objects that are no longer being used by the application. 
        // If GC considers an object eligible for finalization, it calls the finalizer (if any)
        // and reclaims the memory used to store the object.

        //In.NET Framework applications(but not in .NET Core applications), 
        // finalizers are also called when the program exits.
        ~MyObject()
        {
            //#### Cleanup statements here to free resources #####
            // A class can have only one finalizer
            // Finaliazer has no modifier no args and can not be inherited.
            sw.Stop();
            Console.WriteLine("This instance of {0} has been in existence for {1}",
                        this, sw.Elapsed);
        }

        // A call to a finalizer is implicitly translated to the following code:
        // C# provides a destructor instead of overriding the Finalize method.

        //The below code will not compile. The compiler will recommand you to implement a destructor 
        //protected override void Finalize()
        //{
        //    try
        //    {
        //        //cleanup Statements here
        //    }
        //    finally
        //    {
        //        base.Finalize();
        //    }
        //}
    }
}