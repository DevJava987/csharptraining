﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.Ref
{
    // With C# 7.0 or later, ref returns are supported.
    // Ref Returns are method return values by references. 
    // Similar to ref values passed as method arguments, ref returns can be modified by the caller,
    // and any changes (assignments) to the value will reflect to the original value returned from the method.

    //In C#, you can make a ref return using the return ref keywords.
    class RefReturns<T>
    {
        public static ref int ElementAt(ref int[] array, int position)
        {
            return ref array[position];
        }

        private static void Main(string[] args)
        {
            int[] data = new int[10];//initialiazed to ZERO
            Console.WriteLine($"Before change, element at 2 is: {data[2]}");
            ref int value = ref ElementAt(ref data, 2);
            // Change the ref value.
            value = 5;
            Console.WriteLine($"After change, element at 2 is: {data[2]}");
            // RESULT OF THIS PROGRAM:
            // Before change, element at 2 is: 0
            // After change, element at 2 is: 5

            // You can also call a method that has ref return without specifing a "local ref" (i.e variable defined as ref)
            // in this case, the value is return by value.
            int value2 = ElementAt(ref data, 2);
            //In this case the RESULT OF THIS PROGRAM 
            // Before change, element at 2 is: 0
            // After change, element at 2 is: 0

            //PAY ATTENTION: you need either specify ref on both sides or not have ref in both sides.
            //you can not specify ref on one side and not specify it on the other side
            //For example, the following statements will not compile:
            //int value = ref ElementAt(data, 2);
            //int ref value = ElementAt(data, 2);


            //OVERLOADING
            //The return type is not part of the signature of a method.
            //The compiler will not accept this kind of overloading
            // public int method(object param);
            // public ref int method(object param);

            // VALID OVERLOADING:
            //- use a different number of parameter
            //- OR use a different type of parameter
            //- OR use different value passing parameter
            //The following OVERLOADING is valid:
            //public ref int method(int[] data);
            //public ref int method (ref int[] data)


            //REMARQUES ABOUT ARRAYS
            // Arrays are mechanisms that allow you to treat several items as a single collection.
            // The Microsoft® .NET Common Language Runtime (CLR) supports single-dimensional arrays,
            // multidimensional arrays, and jagged arrays (arrays of arrays).
            // All array types are implicitly derived from System.Array, which itself is derived
            // from System.Object. 
            // --> This means that all arrays are always reference types which are allocated on the managed heap,
            // and your app's variable contains a reference to the array and not the array itself.

            //MEMORY & value type Vs Reference type
            // Value types are stored in Stack (Derived from System.ValueType), such as structures in C#
            // Reference types are stored in Heap (Derived from System.Object),

        }

    }
}