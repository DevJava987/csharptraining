﻿using System;
using System.Threading;

namespace Tests.ReadWriteLock
{
    // ReaderWriterLock is used to synchronize access to a resource. 
    // At any given time, it allows either CONCURRENT read access for 
    // MULTIPLE threads, or write access for a SINGLE thread. 
    // In a situation where a resource is changed INfrequently,
    // a ReaderWriterLock provides better throughput than a simple one-at-a-time lock, such as Monitor.

    // ReaderWriterLock works best where most accesses are reads, while writes are infrequent and of short duration.
    // Multiple readers alternate with single writers, so that neither readers nor writers are blocked for long periods.

    //ReaderWriterLock is a sealed class
    class ReaderWriterLockExample
    {

        //Define the shared (STATIC) resource that should be protected by the ReaderWriterLock
        static int resource = 0;
        //Should be static (independent from the current instance)
        static ReaderWriterLock rwLock = new ReaderWriterLock();

        const int numThreads = 26;
        static bool running = true;
        static Random rnd = new Random();

        // Statistics. These variables are shared between many threads.
        // Ti increment/decrement them we will do it atomically thanks to Interlocked
        static int readerTimeouts = 0;
        static int writerTimeouts = 0;
        static int reads = 0;
        static int writes = 0;



        ~ReaderWriterLockExample()
        {
            //Free resources here
            //This method is called when the object is garbage collected!
        }

        // Request and release a reader lock, and handle time-outs.
        static void ReadFromResource(int timeOut)
        {
            try
            {
                // If the timeout is reached without obtaining the lock, ApplicationException is throwed
                // A rwLock is always acquired with a timout defined whether in Millis or in timeSpan            
                // Possible values of the timeout: 
                // 1) -1 to wait indefinitely until obtaining the lock 
                // 2) 0 to not wait. If the lock can not be acqired immediately, the method returns
                // 3) >0 The number of millis to wait
                rwLock.AcquireReaderLock(timeOut);
                try
                {
                    // It is safe for this thread to read from the shared resource.
                    Display("reads resource value " + resource);
                    Interlocked.Increment(ref reads);
                }
                finally
                {
                    // Ensure that the lock is released.
                    rwLock.ReleaseReaderLock();
                }
            }
            catch (ApplicationException)
            {
                // The reader lock request timed out.
                Interlocked.Increment(ref readerTimeouts);
            }
        }

        // Request and release the writer lock, and handle time-outs.
        static void WriteToResource(int timeOut)
        {
            try
            {
                rwLock.AcquireWriterLock(timeOut);
                try
                {
                    // It's safe for this thread to access from the shared resource.
                    resource = rnd.Next(500);
                    Display("writes resource value " + resource);
                    Interlocked.Increment(ref writes);
                }
                finally
                {
                    // Ensure that the lock is released.
                    rwLock.ReleaseWriterLock();
                }
            }
            catch (ApplicationException)
            {
                // The writer lock request timed out.
                Interlocked.Increment(ref writerTimeouts);
            }
        }


        // Requests a reader lock, upgrades the reader lock to the writer
        // lock, and downgrades it to a reader lock again.
        static void UpgradeDowngrade(int timeOut)
        {
            try
            {
                rwLock.AcquireReaderLock(timeOut);
                try
                {
                    // It's safe for this thread to read from the shared resource.
                    Display("reads resource value " + resource);
                    Interlocked.Increment(ref reads);

                    // To write to the resource, either release the reader lock and
                    // request the writer lock, or upgrade the reader lock. Upgrading
                    // the reader lock puts the thread in the write queue, BEHIND any
                    // other threads that might be waiting for the writer lock.
                    try
                    {
                        // LockCookie enables us to save the state of lock before upgrading it
                        // LockCookie is structure (value type)
                        LockCookie lockCookie = rwLock.UpgradeToWriterLock(timeOut);
                        try
                        {
                            // It's safe for this thread to read or write from the shared resource.
                            resource = rnd.Next(500);
                            Display("writes resource value " + resource);
                            Interlocked.Increment(ref writes);
                        }
                        finally
                        {
                            // Ensure that the lock is released.
                            // To get back to the previous state, we need to have the lock state saved before the upgrade
                            rwLock.DowngradeFromWriterLock(ref lockCookie);
                        }
                    }
                    catch (ApplicationException)
                    {
                        // The upgrade request timed out.
                        Interlocked.Increment(ref writerTimeouts);
                    }

                    // If the lock was downgraded, it's still safe to read from the resource.
                    Display("reads resource value " + resource);
                    Interlocked.Increment(ref reads);
                }
                finally
                {
                    // Ensure that the lock is released.
                    // Contrary to releaseLock() method ReleaseReaderLock/ReleaseWriterLock
                    // will NOT return a LockCookie object
                    rwLock.ReleaseReaderLock();
                }
            }
            catch (ApplicationException)
            {
                // The reader lock request timed out.
                Interlocked.Increment(ref readerTimeouts);
            }
        }

        // Release all locks and later restores the lock state.
        // Uses sequence numbers to determine whether another thread has
        // obtained a writer lock since this thread last accessed the resource.
        static void ReleaseRestore(int timeOut)
        {
            int lastWriter;
            try
            {
                rwLock.AcquireReaderLock(timeOut);
                try
                {
                    // It's safe for this thread to read from the shared resource,
                    // so read and cache the resource value.
                    int resourceValue = resource;     // Cache the resource value.
                    Display("reads resource value " + resourceValue);
                    Interlocked.Increment(ref reads);

                    // Save the current writer sequence number.
                    lastWriter = rwLock.WriterSeqNum;

                    // Release the lock and save a cookie so the lock can be restored later.
                    LockCookie lc = rwLock.ReleaseLock();

                    // Wait for a random interval and then restore the previous state of the lock.
                    Thread.Sleep(rnd.Next(250));
                    rwLock.RestoreLock(ref lc);

                    // Check whether other threads obtained the writer lock in the interval.
                    // If not, then the cached value of the resource is still valid.
                    if (rwLock.AnyWritersSince(lastWriter))
                    {
                        resourceValue = resource;
                        Interlocked.Increment(ref reads);
                        Display("resource has changed " + resourceValue);
                    }
                    else
                    {
                        Display("resource has not changed " + resourceValue);
                    }
                }
                finally
                {
                    // Ensure that the lock is released.
                    rwLock.ReleaseReaderLock();
                }
            }
            catch (ApplicationException)
            {
                // The reader lock request timed out.
                Interlocked.Increment(ref readerTimeouts);
            }
        }


        // Helper method briefly displays the most recent thread action.
        static void Display(string msg)
        {
            Console.Write("Thread {0} {1}.       \r", Thread.CurrentThread.Name, msg);
        }
    }
}
