﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Tests.BlockingQueue
{
    class CustomBlockingQueue<T>
    {
        private readonly int size;
        private readonly Queue<T> queue = new Queue<T>();

        //Using expression Body for consutructors
        public CustomBlockingQueue(int size) => this.size = size;

        public void Enqueue(T item)
        {
            lock (queue)//Monitor.Enter(queue)
            {
                while (queue.Count >= size)
                {
                    Monitor.Wait(queue);
                }
                queue.Enqueue(item);
                Monitor.PulseAll(queue);
            }//Monitor.Exit(queue); (in finaly bloc)
        }

        public T Dequeue()
        {
            lock (queue)//equivalent to Monitor.Enter(queue)
            {
                //Monitor.IsEntered(queue);
                while (size == 0)
                {
                    Monitor.Wait(queue);
                }
                Monitor.PulseAll(queue);
                return queue.Dequeue();
            }
        }
    }
}
