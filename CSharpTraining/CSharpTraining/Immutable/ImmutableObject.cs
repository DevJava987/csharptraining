﻿using System.Diagnostics;


namespace Tests.Immutable
{
    // Structs and classes are not immutable by default.
    // However, it is a best practice to make structs immutable (why?), specially in a multithreaded context!

    // - Immutable objects are good in a multithreaded context: you can share objects 
    // between different threads without worring about stale/inconsistent data
    // - Immutable object are good candidates for being used as a key in a Dictionary


    //-1 The class should be sealed to prevent modifying it by inheritance
    // 2- No setter: readonly properties
    //-3 Properties should be declared private and their reference not exported through a getter for example.
    // If you want to export the property make the property itself immutable (type string for example) or
    // return a copy to the caller context.
    //-4 - Avoid indirectly exporting an attribute or a property, through constructor for example (defensive copy)
    //-5 avoid having methods that mutate properties. Since the latters are declared as readonly we will get a compile error
    //if a method tries to modify a properties. Have a look at "MakeSomeStuff" method

    sealed class ImmutableObject
    {
        //What'is the difference between property and simple attribut

        public ImmutableObject(string immutableProp, Stopwatch stopwatch)
        {
            ImmutableProperty = immutableProp;
            SW = stopwatch;
        }

        // The reference of this property can be exported (by a getter)  because
        // it is immutable, the caller can not modify it!
        //Note that the following syntax of "readonly property" was introduced in C# 6.0
        private string ImmutableProperty { get; }

        // Exporting the property as below will enable caller to modify it
        // by calling StopWatch.Reset/Restart for example
        // Make sure that an object is immutable if you want to export it reference.
        // Otherwise, you should export a copy
        // This property does not comply with "immutable concept"
        private Stopwatch SW { get; }

        public void MakeSomeStuff()
        {
            // this is not possible since it is a readonly property
            //ImmutableProperty = "new value while runime";

            // PAY ATTENTION: Although it a readonly property, this is property can be modified
            // since it is mutable object.
            Stopwatch.StartNew();

        }
    }

    //### DIFFERENCES "const" Vs "readonly"
    //1- const fields should be initialized at declaration while readonly ones
    // can be initialized at declaration or by the constructors 
    //2- const field value is determined at compilation time while readonly field value is determined at runtime
    // for this second reason "readonly" field are more flexible
}