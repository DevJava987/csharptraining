﻿
using System.Collections.Generic;
using System.Threading;

// It is legal for the same thread to invoke Enter more than once without being blocked (reentrancy!);
// However, an equal number of Exit calls must be invoked before other threads waiting 
// on the object will unblock.
// The Monitor is reentrant!
namespace Tests.LockAndMonitor
{
    class SafeQueue<T>
    {

        private readonly Queue<T> queue;

        public SafeQueue() => queue = new Queue<T>();

        // Request the lock, and block until it is obtained.
        public void Enqueue(T item)
        {
            Monitor.Enter(queue);
            try
            {
                queue.Enqueue(item);
            }
            finally
            {
                Monitor.Exit(queue);
            }
        }

        // To simplify the code of "Monitor.Enter try catch Monitor.Exit", we can use lock syntax.
        // the lock is the "idiot-proof" of the Monitor construct 
        public void SimplifiedEnqueue(T item)
        {
            lock (queue)
            {
                queue.Enqueue(item);
            }
        }


        //Add an element to the queue only if the queue is IMMEDIATELY obtained: NO WAIT!
        public bool TryEnqueue(T item)
        {
            if (Monitor.TryEnter(queue))
            {
                try
                {
                    queue.Enqueue(item);
                }
                finally
                {
                    Monitor.Exit(queue);
                }
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool TryEnqueue(T item, int waitTimeInMillis)
        {
            if (Monitor.TryEnter(queue, waitTimeInMillis))
            {
                try
                {
                    queue.Enqueue(item);
                }
                finally
                {
                    Monitor.Exit(queue);
                }
                return true;
            }
            else
            {
                return false;
            }
        }


        public T Dequeue()
        {
            Monitor.Enter(queue);
            try
            {
                return queue.Dequeue();
            }
            finally
            {
                Monitor.Exit(queue);
            }
        }

        // If the lock was not taken because an exception was thrown, 
        // the variable specified for the lockTaken parameter is false 
        // after this method ends. 
        // This allows the program to determine, in all cases, whether 
        // it is necessary to release the lock. 
        // If this method returns without throwing an exception,
        // the variable specified for the lockTaken parameter is always true,
        // and there is no need to test it.
        public T AdvancedDequeue()
        {
            bool lockTaken = false;
            try
            {
                //Note the usage of ref here!
                Monitor.Enter(queue, ref lockTaken);
                //If we reach this stage, this means there was no exception.
                //So there is no need to test lockTaken since it is always true in case of no exception
                return queue.Dequeue();
            }
            finally
            {
                if (lockTaken)
                {
                    Monitor.Exit(queue);
                }
            }
        }
    }
}
