﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.Nullable
{
    // C# 2.0 introduced nullable types that allow you to assign null to value type variables. 
    // You can declare nullable types using Nullable<t> where T is a value type.
    // A nullable type can represent the correct range of values for its underlying value type,
    // plus an additional null value. 
    // For example, Nullable<int> can be assigned any value from -2147483648 to 2147483647, or a null value.

    // have a lokk at this link: https://www.tutorialsteacher.com/csharp/csharp-nullable-types
    class Nullable
    {
        // a value type can not be assigned a null value (even structure?)
        //for example, in the following line you will get a compile error
        // int i = null;
        static void Main(String[] args)
        {
            Nullable<int> i = null;// a shorthand for this is: "int i ? = null;"
            if (i.HasValue)
            {
                // If this is called when "i.HasValue == false", it will throw an exception at runtime
                // InvalidOperationException (SystemException)
                // If you are note sure if a value has been set, you can use GetValueOrDefault() 
                // or GetValueOrDefault(T defaultValue)
                Console.WriteLine(i.Value); // or Console.WriteLine(i)
            }
            else
            {
                Console.WriteLine("Null");
            }
        }
    }
}
