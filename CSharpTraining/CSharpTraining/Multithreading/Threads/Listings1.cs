﻿using System;
using System.Linq;
using System.Threading;

namespace Tests.Multithreading.Threads
{
    class Listing1
    {
        public void Execute()
        {
            //#### Different ways to launch a thread without a param ####
            Thread userThread = new Thread(new ThreadStart(DoSomeWork));
            Thread lambdaUserThread = new Thread(() =>
            {
                foreach (var value in Enumerable.Range(0, 10))
                {
                    Console.WriteLine($"LAMBDA User Thread say : {value}");
                    Thread.Sleep(110);
                }
            });
            Thread anonymousDelegateThread = new Thread(delegate ()
            {
                DoSomeWork();
            });
            userThread.Start();
            lambdaUserThread.Start();
            anonymousDelegateThread.Start();

            //#### Different way to launch threads with one parameter
            Thread parmaUserThread = new Thread(new ParameterizedThreadStart(DoSomeWorkWithParam));
            Thread lambdaWithParamsUserThread = new Thread((object param) =>
            {
                int threshold = (int)param;
                foreach (var value in Enumerable.Range(0, threshold))
                {
                    Console.WriteLine($"LAMBDA User Thread say : {value}");
                    Thread.Sleep(110);
                }
            });
            Thread anonymousDelegateWithParamThread = new Thread(delegate (object param)
            {
                string castedParam = param as string;
                DoSomeWork();
            });

            parmaUserThread.Start("param IN");
            lambdaWithParamsUserThread.Start(10);
            anonymousDelegateWithParamThread.Start("param IN");


            Console.WriteLine("Main thread is running");
            foreach (var value in Enumerable.Range(0, 10))
            {
                Console.WriteLine($"Main Thread say : {value}");
                Thread.Sleep(100);
            }
            userThread.Join();
            parmaUserThread.Join();
            lambdaUserThread.Join();
            anonymousDelegateThread.Join();
        }

        private void DoSomeWork()
        {
            foreach (var value in Enumerable.Range(0, 10))
            {
                Console.WriteLine($"User Thread say : {value}");
                Thread.Sleep(110);
            }
        }

        private void DoSomeWorkWithParam(object param)
        {
            foreach (var value in Enumerable.Range(0, 10))
            {
                Console.WriteLine($"PARAMETERIZED User Thread say : {value}");
                Thread.Sleep(110);
            }
        }

    }
}
