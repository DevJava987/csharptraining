﻿using System;
using System.Threading;

namespace Tests.Multithreading.StoppingThreads
{
    class MainProcess
    {

        // Keyword volatile is used as a hint to the compiler that this data
        // member is accessed by multiple threads.
        // The compiler, the runtime system and even the hardware may re-arrange
        // reads and writes to memory locations for performance reasons.
        // Using volatile keyword ensures that all threads reads volatile writes 
        // in the order in which they are performed.
        private volatile bool stop = false;

        public void Process()
        {
            //Without the volatile keyword, the behaviour is unpredectible.
            //Not using the volatile modifier may result in reading stale data
            //Because of the nature of multi-threaded programming, the number of 
            //stale reads is unpredictable. Also, different runs will produce somewhat different results.
            while (!stop)
            {
                Console.WriteLine("MainProcess executing...");
                Thread.Sleep(1);
            }
            Console.WriteLine("Leaving the MainProcess...");
        }

        public void Stop()
        {
            this.stop = true;
        }
    }
}