﻿using System;
using System.Diagnostics;
using System.Threading;

namespace Tests.Multithreading.StoppingThreads
{
    class AnotherProcess
    {
        private MainProcess mainThread;

        public AnotherProcess(MainProcess mainThreadToBeStopped) => mainThread = mainThreadToBeStopped;

        public void Process()
        {
            var sw = Stopwatch.StartNew();
            var elapsedTimeMillis = sw.ElapsedMilliseconds;
            while (elapsedTimeMillis < 1000)
            {
                Console.WriteLine("AnotherProcess is executing...");
                Thread.Sleep(15);
                elapsedTimeMillis = sw.ElapsedMilliseconds;
            }
            Console.WriteLine("Stopping the MainProcess...");
            mainThread.Stop();
            Console.WriteLine("AnotherProcess Finished...");
        }
    }
}
