﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Tests.Multithreading.StoppingThreads
{
    class App
    {

        public void Execute()
        {
            MainProcess mp = new MainProcess();
            Thread mainThRead = new Thread(new ParameterizedThreadStart(RunMainThread));
            Thread anotherThread = new Thread(new ParameterizedThreadStart(RunAnotherThread));

            mainThRead.Start(mp);
            anotherThread.Start(mp);

            mainThRead.Join();
            anotherThread.Join();
        }

        private static void RunMainThread(object mainProcess)
        {
            //((MainProcess)mainProcess).Process();
            MainProcess mp = mainProcess as MainProcess;
            mp.Process();
        }

        private static void RunAnotherThread(object mainProcessToBeStopped)
        {
            MainProcess mainprocess = (MainProcess)mainProcessToBeStopped;
            AnotherProcess at = new AnotherProcess(mainprocess);
            at.Process();
        }
    }
}
