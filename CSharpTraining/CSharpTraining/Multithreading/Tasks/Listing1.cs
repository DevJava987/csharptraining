﻿using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System;

namespace Tests.Multithreading.Tasks
{
    class Listing1
    {

        public void Execute()
        {
            /**
             * Example of tasks witht no return value neither a parameter
             * In this case, Action delegates are used
             **/

            Task userTask2 = new Task(new Action(DoSomeWork));

            //With Lambda expression
            Task userTask3 = new Task(() =>
            {
                foreach (var value in Enumerable.Range(1, 30))
                {
                    Console.WriteLine($"Task3 running {value}");
                    Thread.Sleep(100);
                }
            });

            //With anonymous delegate
            Task userTask4 = new Task(delegate ()
            {
                foreach (var value in Enumerable.Range(1, 30))
                {
                    Console.WriteLine($"Task3 (anonymous delelagte) running {value}");
                    Thread.Sleep(100);
                }
            });

            userTask1.Start();
            userTask2.Start();
            userTask3.Start();
            userTask4.Start();

            // Examples of Tasks that return an object but do not need parameters 
            //In this case, we need to use Func delegates
            Task<int> userTaskWithReturnValue1 = new Task<int>(DoSomeWorkWithReturnValue);
            Task<int> userTaskWithReturnValue2 = new Task<int>(new Func<int>(DoSomeWorkWithReturnValue));
            Task<int> userTaskWithReturnValue3 = new Task<int>(() =>
            {
                int counter = 0;
                foreach (var value in Enumerable.Range(1, 30))
                {
                    Console.WriteLine($"Task running {value}");
                    counter++;
                    Thread.Sleep(100);
                }
                return counter;
            });

            //With Anonymous delegate
            Task<int> userTaskWithReturnValue4 = new Task<int>(delegate ()
            {
                int counter = 0;
                foreach (var value in Enumerable.Range(1, 30))
                {
                    Console.WriteLine($"Task running {value}");
                    counter++;
                    Thread.Sleep(100);
                }
                return counter;
            });

            userTaskWithReturnValue1.Start();
            userTaskWithReturnValue2.Start();
            userTaskWithReturnValue3.Start();
            userTaskWithReturnValue4.Start();
            Console.WriteLine($"Resut for the first task is {userTaskWithReturnValue1.Result}");
            Console.WriteLine($"Resut for the second task is {userTaskWithReturnValue2.Result}");
            Console.WriteLine($"Resut for the third task is {userTaskWithReturnValue3.Result}");
            Console.WriteLine($"Resut for the forth task is {userTaskWithReturnValue4.Result}");

            //A Func is used when your delegate has a return value (and optionally in a parameter). 
            //An Action is fit when there is no return value.
            Func<object, string> funcDelegate = DoSomeWorkWithReturnValueAndParams;
            DoSomeWorkWithReturnValueAndParams("Such delegate can be call classically as a method");

            object obj1 = new object();
            object obj2 = new object();
            Object obj3 = new Object();
            Task<string> customTask1 = new Task<string>(() => funcDelegate(obj1));
            Task<string> customTask2 = new Task<string>(() => DoSomeWorkWithReturnValueAndParams(obj2));

            Task<string> customTask3 = new Task<string>(delegate ()
            {
                return DoSomeWorkWithReturnValueAndParams("coucou");
            });



            //Task(Func<object, TResult> function, object state);
            object state = new object();
            object obj = new object();
            Task<string> customTask4 = new Task<string>((new Func<object, String>(DoSomeWorkWithReturnValueAndParams)), state);
            //The equivalent but simplified
            //Task<string> customTask5 = new Task<string>(DoSomeWorkWithReturnValueAndParams, state);
            customTask4.Start();

            Task<string> customTask5 = new Task<string>(DoSomeWorkWithReturnValueAndParams, state);

            //Task.Run<String>(DoSomeWorkWithReturnValueAndParams(""));
            Console.WriteLine("End main Thread");
        }

        private void DoSomeWork()
        {
            foreach (var value in Enumerable.Range(1, 30))
            {
                Console.WriteLine($"Task running {value}");
                Thread.Sleep(100);
            }
        }


        private int DoSomeWorkWithReturnValue()
        {
            int counter = 0;
            foreach (var value in Enumerable.Range(1, 30))
            {
                Console.WriteLine($"Task running {value}");
                counter++;
                Thread.Sleep(100);
            }
            return counter;
        }


        private string DoSomeWorkWithReturnValueAndParams(object param)
        {
            Console.WriteLine("START the ReturnAndParam task...");
            Thread.Sleep(1000);
            Console.WriteLine("END the ReturnAndParam task...");
            return "toto";
        }
    }
}
