﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.Event
{
    public delegate void MyEventDelegate();

    class Publisher
    {
        public event MyEventDelegate MyEvent;
        public void Process()
        {
            Console.WriteLine("Publisher is running");

            //Now publish the event
            MyEvent();
        }
    }
}
