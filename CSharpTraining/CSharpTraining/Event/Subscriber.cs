﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.Event
{
    class Subscriber
    {
        private Publisher Publisher { get; set; }

        public Subscriber(Publisher publisher)
        {
            Publisher = publisher;

            //This is how to subscribe to the publisher 
            //by assigning the handler to the event delegate of the publisher
            Publisher.MyEvent += HandleTheEvent;
        }

        //This method will handle the event raised by the publisher
        //The method should match the signature of the delegate event defined by the publisher
        public void HandleTheEvent()
        {
            //Here, the subscriber can handle the event raised by the publisher
        }

    }
}
