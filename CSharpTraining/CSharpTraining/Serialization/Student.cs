﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Tests.Serialization
{
    // It is important to note that the Serializable attribute cannot be inherited. 
    // If you derive a new class from Student, the new class must be marked with the attribute as well,
    // or it cannot be serialized.
    [Serializable]
    class Student
    {
        public int ID;
        public string Name;

        [NonSerialized]
        private string CreditCardNumber;

        public void Execute()
        {
            Student student = new Student() { ID = 99, Name = "John", CreditCardNumber = "879116" };

            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(@"C:\ExampleSerialization.txt", FileMode.Create, FileAccess.Write);

            formatter.Serialize(stream, student);
            stream.Close();

            stream = new FileStream(@"C:\ExampleSerialization.txt", FileMode.Open, FileAccess.Read);
            Student newStudent = (Student)formatter.Deserialize(stream);

            Console.ReadKey();
        }
    }
}