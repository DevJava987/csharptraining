﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Tests.AsyncAwait
{
    class App1
    {
        public void Execute()
        {
            Console.WriteLine($"Result is {DoMainWork()}");
            Console.ReadLine();
        }

        //- The name of the method should finish with "Async"
        //- The method should have the "async" modifier
        //- returns Task<T>
        private async Task<int> DoSomeHardWorkAsync()
        {
            // statments before calling the "await..." are executed in a synchronous way
            // once the "await..." is called this method will return immediately and give hand 
            // to the caller method. At the same time task (and following statments) will 
            // be exectued asycnhronously 
            int result = await Task<int>.Run<int>(new Func<int>(() =>
            {
                foreach (var i in Enumerable.Range(1, 10))
                {
                    Thread.Sleep(1000);
                    Console.WriteLine("Async job still ongoing...");
                }
                return 5;
            }));
            return result;
        }

        private int DoMainWork()
        {
            Console.WriteLine("START main Thread, caller method...");
            Task<int> asycnTask = DoSomeHardWorkAsync();

            Console.WriteLine("Do some work item of the main job...");
            Thread.Sleep(3000);

            Console.WriteLine("Waiting for Asycn work to finish...");
            int result = asycnTask.Result;
            Console.WriteLine("Got The result of the Asycn work...");

            return result * 2;
        }

        //public void Execute()
        //{
        //    Task<int> task = HandleFileAsync();

        //    // Control returns here before HandleFileAsync returns.
        //    // ... Prompt the user.
        //    Console.WriteLine("Back in the main worker");
        //    for (int i = 0; i < 10; i++)
        //    {
        //        Console.WriteLine("Hi from the main worker");
        //        Thread.Sleep(100);
        //    }

        //    // Wait for the HandleFile task to complete.
        //    // ... Display its results.
        //    Console.WriteLine("Waiting for the async worker to finish");
        //    task.Wait();
        //    var x = task.Result;
        //    Console.WriteLine("Count: " + x);

        //    Console.WriteLine("[DONE]");
        //    Console.ReadLine();
        //}

        static async Task<int> HandleFileAsync()
        {
            string file = @"C:\Dev\file.txt";
            Console.WriteLine("HandleFile enter");
            int count = 0;

            for (int i = 0; i < 15; i++)
            {
                Console.WriteLine("Hi Before launching ASYNC Working");
                Thread.Sleep(100);
            }

            // Read in the specified file.
            // ... Use async StreamReader method.
            using (StreamReader reader = new StreamReader(file))
            {
                string v = await reader.ReadToEndAsync();

                //Console.WriteLine("Finished exeting the awaitable method ReadToEndAsync");
                count += v.Length;

                for (int i = 0; i < 30; i++)
                {
                    Console.WriteLine("Hi from the asynch worker");
                    Thread.Sleep(100);
                }
            }
            Console.WriteLine("HandleFile exit");
            return count;
        }
    }

}