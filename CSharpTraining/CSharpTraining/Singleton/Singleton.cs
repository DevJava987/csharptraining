﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.Singleton
{
    class Singleton
    {
        //this field should be static
        private static Singleton _singleton;

        //private constructor
        private Singleton()
        {
        }

        //static constructor (always public)
        static Singleton() { }

        public static Singleton Instance
        {
            get
            {
                if (_singleton == null)
                {
                    _singleton = new Singleton();
                }
                return _singleton;
            }
        }

    }
}
