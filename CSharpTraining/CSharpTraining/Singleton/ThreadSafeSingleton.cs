﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.Singleton
{
    class ThreadSafeSingleton
    {
        private static ThreadSafeSingleton _instance;

        private ThreadSafeSingleton()
        {
        }

        static ThreadSafeSingleton() { }

        public static ThreadSafeSingleton Instance
        {
            get
            {
                lock (_instance)
                {
                    if (_instance == null)
                    {
                        _instance = new ThreadSafeSingleton();
                    }
                    return _instance;
                }
            }
        }
    }
}
