﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.Singleton
{
    class EagerSingleton
    {
        //instatiated when the class is loaded by the class loader
        private static readonly EagerSingleton _instance = new EagerSingleton();

        //private constructor
        private EagerSingleton() { }

        //static constructor (always public, you cannot put another modifier here)
        //The static constructor cannot explicitly called!
        static EagerSingleton() { }

        public static EagerSingleton Instance
        {
            get { return _instance; }
        }

        public void Process()
        {
            //Do something here;
        }

        public static void Main()
        {
            EagerSingleton.Instance.Process();
        }
    }
}
